package com.ecommerce.dao;

import java.util.List;
import javax.persistence.EntityManager;
import org.springframework.stereotype.*;

import org.hibernate.query.*;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.*;

import com.ecommerce.model.Product;
import com.ecommerce.model.User;

@Repository
public class AdminDAOImpl implements AdminDAO {
	
	@Autowired
	private EntityManager entityManager;

	@Override
	public List<Product> allProducts() {
		Session currentSession=entityManager.unwrap(Session.class);
		String s="from Product";
		Query<Product> query =currentSession.createQuery(s,Product.class);
	 List<Product> products= query.getResultList();
		return products;
	}

	@Override
	public Product getProductById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addProduct(Product product) {
		Session currentSession=entityManager.unwrap(Session.class);
		   currentSession.save(product);
	
	}

	@Override
	public void deleteProduct(int id) {
		Session currentSession=entityManager.unwrap(Session.class);
		  Product product= currentSession.get(Product.class, id);
		  System.out.println("pid"+product.getId());
		  currentSession.delete(product);
	}

	@Override
	public void updateProduct(Product product) {
		Session currentSession=entityManager.unwrap(Session.class);
		   currentSession.update(product);
	}

}
