package com.ecommerce.dao;

import java.util.List;

import com.ecommerce.model.Product;


public interface AdminDAO {
	 List<Product> allProducts();
	 Product getProductById(int id);
	 void addProduct(Product product);
	 void deleteProduct(int id);
	 void updateProduct(Product product);
}
