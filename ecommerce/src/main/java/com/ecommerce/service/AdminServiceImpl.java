package com.ecommerce.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.*;
import org.springframework.transaction.annotation.Transactional;

import com.ecommerce.dao.AdminDAO;
import com.ecommerce.model.Product;

@Service
public class AdminServiceImpl implements AdminService {

	@Autowired
	private AdminDAO adminDAO;
	
	@Override
	public List<Product> allProducts() {
		
		return adminDAO.allProducts();
	}

	@Override
	public Product getProductById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Transactional
	@Override
	public void addProduct(Product product) {
		// TODO Auto-generated method stub
		 adminDAO.addProduct(product);

	}
	@Transactional
	@Override
	public void deleteProduct(int id) {
		 System.out.println("id"+id);
		   adminDAO.deleteProduct(id);

	}

	@Transactional
	@Override
	public void updateProduct(Product product) {
		adminDAO.updateProduct(product);
		
	}

}
