package com.ecommerce.service;

import java.util.List;

import com.ecommerce.model.Product;

public interface AdminService {

	 List<Product> allProducts();
	 Product getProductById(int id);
	 void addProduct(Product product);
	 void deleteProduct(int id);
	 void updateProduct(Product product);
	
}
