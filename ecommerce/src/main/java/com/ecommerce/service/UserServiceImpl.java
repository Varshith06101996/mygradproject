package com.ecommerce.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.*;
import org.springframework.transaction.annotation.Transactional;

import com.ecommerce.dao.UserDAO;
import com.ecommerce.model.User;

@Service
public class UserServiceImpl implements UserService {

	
	@Autowired
	private UserDAO userDAO;
	
	@Transactional
	@Override
	public List<User> get() {
		return userDAO.get();
	}

	@Transactional
	@Override
	public User get(int id) {
		   
		return userDAO.get(id);
	}
	@Transactional
	@Override
	public void registerUser(User user) {
		// TODO Auto-generated method stub
		 userDAO.registerUser(user);
		
	}
	
	@Transactional
	@Override
	public void deleteUser(int id) {
		System.out.println("ID"+id);
		// TODO Auto-generated method stub
		userDAO.deleteUser(id);
		
	}

	@Override
	public String login(User user) {
		// TODO Auto-generated method stub
		 return userDAO.login(user);
		
	}

}
