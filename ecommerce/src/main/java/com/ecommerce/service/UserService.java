package com.ecommerce.service;

import java.util.List;

import com.ecommerce.model.User;

public interface UserService {

	 List<User> get();
	 User get(int id);
	 void registerUser(User user);
	 void deleteUser(int id);
	 String login(User user);
	
}
