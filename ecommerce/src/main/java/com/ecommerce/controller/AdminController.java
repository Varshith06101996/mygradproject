package com.ecommerce.controller;


import org.springframework.web.bind.annotation.*;

import com.ecommerce.model.Product;
import com.ecommerce.service.AdminService;

import org.springframework.beans.factory.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/admin")
public class AdminController {

	@Autowired
	 private AdminService adminService;
	 
	@CrossOrigin
	@PostMapping("/addProduct")
	 public Product addProduct(@RequestBody Product product) {
		 adminService.addProduct(product);
		   return product;
	 }
	
	@CrossOrigin
	@GetMapping("/allProducts")
	public List<Product> getAllProducts(){
		
		return adminService.allProducts();
		
	}
	
	@CrossOrigin
	@DeleteMapping("/deleteProductById/{id}")
	public void deleteProduct(@PathVariable int id) {
		System.out.println("Inside delete user");
		 adminService.deleteProduct(id);
		
	}
	@CrossOrigin
	@PutMapping("/updateProduct")
	 public Product updateProduct(@RequestBody Product product) {
		 adminService.updateProduct(product);
		   return product;
	 }
	
}
