package com.ecommerce.controller;

import org.springframework.web.bind.annotation.*;

import com.ecommerce.model.User;
import com.ecommerce.service.UserService;

import org.springframework.beans.factory.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/api")
public class UserController {

	
	@Autowired
	private UserService userService;
	
	@CrossOrigin
	@GetMapping("/allusers")
	public List<User> get(){
		
		return userService.get();
		
	}
	
	@CrossOrigin
	@PostMapping("/register")
	public User register(@RequestBody User user) {
		userService.registerUser(user);
		return user;
		
	}
	
	
	@GetMapping("/getUserById/{id}")
	public User getUser(@PathVariable int id) {
		
		  return userService.get(id);
		
	}
	
	@DeleteMapping("/deleteUserById/{id}")
	public void deleteUser(@PathVariable int id) {
		System.out.println("Inside delete user");
		 userService.deleteUser(id);
		
	}
	
	@CrossOrigin
	@PostMapping("/login")
	public String loginUser(@RequestBody User user) {
		System.out.println("inside the login user!!");
		return userService.login(user);
	}
	
	
}
