package com.ecommerce.dao;

import java.util.List;

import com.ecommerce.model.User;

public interface UserDAO {
	
	 List<User> get();
	 User get(int id);
	 void registerUser(User user);
	 void deleteUser(int id);
	 String login(User user);

}
