package com.ecommerce.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.springframework.stereotype.*;

import com.ecommerce.model.User;
import org.hibernate.query.*;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.*;


@Repository
public class UserDAOImpl implements UserDAO {
	
	@Autowired
	private EntityManager entityManager;

	@Override
	public List<User> get() {
		// TODO Auto-generated method stub
		Session currentSession=entityManager.unwrap(Session.class);
		String s="from User";
		
		Query<User> query =currentSession.createQuery(s,User.class);
		
	 List<User> users= query.getResultList();
		
		return users;
	}

	@Override
	public User get(int id) {
		
		Session currentSession=entityManager.unwrap(Session.class);
		  User user= currentSession.get(User.class, id);
		  System.out.println("user"+user);
		  return user;
	}

	@Override
	public void registerUser(User user) {
		Session currentSession=entityManager.unwrap(Session.class);
		   currentSession.save(user);
		
	}

	@Override
	public void deleteUser(int id) {
		
		Session currentSession=entityManager.unwrap(Session.class);
		  User user= currentSession.get(User.class, id);
		  System.out.println("user"+user.getId());
		  currentSession.delete(user);
		
		
	}
	@Override
	public String login(User user) {
	Session currentSession=entityManager.unwrap(Session.class);
//		  User userdata= currentSession.get(User.class, user.getUsername());
//		  System.out.println("user"+userdata);
		  //return user;
	System.out.println("password"+ user.getPassword());
	System.out.println("user"+user);
	User u1 = null;
	try {
		u1=(User)currentSession.createQuery("from User where username = :username").setParameter("username",user.getUsername()).getSingleResult();
	}
	catch(NoResultException nre) {
		
	}
		 if(u1 == null) {
			 return "0"; // user not found!
		 }
		 if(u1.getPassword().equals(user.getPassword()))
		 {
			 return "1"; // login success;
		 }
		 else
		 {
			 return "2"; // password incorrect
		 }
		
		
	}

}
